import { Especialidad } from './especialidad';
export class Medico{
    id:number
    run:string
    nombres:string
    apellidos:string
    direccion:string
    telefono:string
    email:string
    especialidadmedicas:Especialidad[]=[]
}