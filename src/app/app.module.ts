import { MaterialModule } from './material/material.module';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PersonaComponent } from './persona/persona.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
/*import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';*/
import { PerroComponent } from './perro/perro.component';
import { EjemploComponent } from './ejemplo/ejemplo.component';
import { ExamenComponent } from './pages/examen/examen.component';
import { EspecialidadComponent } from './pages/especialidad/especialidad.component';
import { HttpClientModule } from '@angular/common/http';
import { ExamenEdicionComponent } from './pages/examen/examen-edicion/examen-edicion.component';
import { EspecialidadEdicionComponent } from './pages/especialidad/especialidad-edicion/especialidad-edicion.component';
import { EspecialistaComponent } from './pages/especialista/especialista.component';
import { EspecialistaEdicionComponent } from './pages/especialista/especialista-edicion/especialista-edicion.component';
import { ConsultaComponent } from './pages/consulta/consulta.component';
import { ConsultaEdicionComponent } from './pages/consulta/consulta-edicion/consulta-edicion.component';
import { LoginComponent } from './pages/login/login.component';
import { PacienteComponent } from './pages/paciente/paciente.component';
import { PacienteEdicionComponent } from './pages/paciente/paciente-edicion/paciente-edicion.component';


@NgModule({
  declarations: [
    AppComponent,
    PersonaComponent,
    PerroComponent,
    EjemploComponent,
    ExamenComponent,
    EspecialidadComponent,
    ExamenEdicionComponent,
    EspecialidadEdicionComponent,
    EspecialistaComponent,
    EspecialistaEdicionComponent,
    ConsultaComponent,
    ConsultaEdicionComponent,
    LoginComponent,
    PacienteComponent,
    PacienteEdicionComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    /*MatButtonModule,
    MatCardModule,
    MatInputModule,*/
    AppRoutingModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
