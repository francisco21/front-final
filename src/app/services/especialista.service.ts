import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HOST, TOKEN_NAME } from './../shared/var.constant';
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Medico } from '../model/especialista';

@Injectable({
  providedIn: 'root'
})
export class EspecialistaService {

  medicoCambio=new Subject<Medico[]>();
  mensajeCambio=new Subject<string>();
  url:string=`${HOST}/medicos`;
  constructor(private http:HttpClient) { }
  listar(){    
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;  
    return this.http.get<Medico[]>(this.url,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  });
  }
  modificar(medico:Medico){  
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;   
    console.log(this.url);
    console.log(medico);
    return this.http.put(this.url,medico,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  });
  }
  registar(medico:Medico){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token; 
    return this.http.post(this.url,medico,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  });
  }
  listarPorId(id:number){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token; 
    return this.http.get<Medico>(`${this.url}/${id}`,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  });   
  }
  eliminar(id:number){   
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;  
    return this.http.delete(`${this.url}/${id}`,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  });
  }
}
