import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HOST, TOKEN_NAME } from './../shared/var.constant';
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Paciente } from '../model/paciente';

@Injectable({
  providedIn: 'root'
})
export class PacienteService {
  pacienteCambio=new Subject<Paciente[]>();
  mensajeCambio=new Subject<string>();
  url:string=`${HOST}/pacientes`;
  constructor(private http:HttpClient) { }
  listar(){    
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;  
    return this.http.get<Paciente[]>(this.url,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  });
  }
  modificar(paciente:Paciente){ 
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;         
    return this.http.put(this.url,paciente,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  });
  }
  registar(paciente:Paciente){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.post(this.url,paciente,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  });
  }
  listarPorId(id:number){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<Paciente>(`${this.url}/${id}`,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  });    
  }
  eliminar(id:number){   
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token; 
    return this.http.delete(`${this.url}/${id}`,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  });
  }
}
