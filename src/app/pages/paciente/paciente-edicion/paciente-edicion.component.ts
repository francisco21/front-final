import { PacienteService } from './../../../services/paciente.service';
import { Paciente } from './../../../model/paciente';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-paciente-edicion',
  templateUrl: './paciente-edicion.component.html',
  styleUrls: ['./paciente-edicion.component.css']
})
export class PacienteEdicionComponent implements OnInit {
  form:FormGroup;
  id:number;
  paciente:Paciente;
  edicion:boolean=false;
  constructor(private route:ActivatedRoute,private router:Router,private pacienteService:PacienteService) { 
    this.form=new FormGroup({
      'id': new FormControl(0),
      'apellidos':new FormControl(''), 
      'direccion':new FormControl(''),
      'email':new FormControl(''),
      'nombres':new FormControl(''),
      'run':new FormControl(''),
      'telefono':new FormControl(''),           
    }); 
  }
  initForm(){
    if(this.edicion){
      this.pacienteService.listarPorId(this.id).subscribe(data=>{
        this.form=new FormGroup({
          'id': new FormControl(data.id),
          'apellidos':new FormControl(data.apellidos),
          'direccion':new FormControl(data.direccion),
          'email':new FormControl(data.email),
          'nombres':new FormControl(data.nombres),
          'run':new FormControl(data.run),
          'telefono':new FormControl(data.telefono),
        });
      })
        
    }
  }
  ngOnInit() {
    this.paciente=new Paciente();
    this.route.params.subscribe((params: Params)=>{
      this.id=params['id'];
      this.edicion=params['id']!=null;
      this.initForm();      
    });
  }
  operar(){
    this.paciente.id=this.form.value['id'];
    this.paciente.apellidos=this.form.value['apellidos'];
    this.paciente.direccion=this.form.value['direccion'];
    this.paciente.email=this.form.value['email'];
    this.paciente.nombres=this.form.value['nombres'];
    this.paciente.run=this.form.value['run'];
    this.paciente.telefono=this.form.value['telefono'];        
    if(this.edicion){

      this.pacienteService.modificar(this.paciente).subscribe(data=>
      {
        this.pacienteService.listar().subscribe(pacientes => {this.pacienteService.pacienteCambio.next(pacientes);       
        this.pacienteService.mensajeCambio.next("Se realizo el cambio de manera exitosa!");
      });
    });
    
    }
    else{
      this.pacienteService.registar(this.paciente).subscribe(data=>{
        this.pacienteService.listar().subscribe(pacientes => {this.pacienteService.pacienteCambio.next(pacientes);       
        this.pacienteService.mensajeCambio.next("Guardado exitoso!");
        });
      });
      
    }
    this.router.navigate(['paciente']);
  }

}