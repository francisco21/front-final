import { ConsultaService } from './../../../services/consulta.service';
import { PacienteService } from './../../../services/paciente.service';
import { EspecialistaService } from './../../../services/especialista.service';
import { MatSnackBar } from '@angular/material';
import { ExamenService } from './../../../services/examen.service';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { ConsultaExamen, ConsultaListaExamen } from './../../../model/consultaexamen';
import { Medico } from './../../../model/especialista';
import { Consulta } from './../../../model/consulta';
import { Especialidad } from './../../../model/especialidad';
import { Examen } from './../../../model/examen';
import { DetalleConsulta } from './../../../model/detalleconsulta';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Paciente } from 'src/app/model/paciente';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-consulta-edicion',
  templateUrl: './consulta-edicion.component.html',
  styleUrls: ['./consulta-edicion.component.css']
})
export class ConsultaEdicionComponent implements OnInit {

  especialista:string='';
  paciente:string='';
  examen:string='';
  examenes:Examen[];  
  medicos:Medico[];
  pacientes:Paciente[];
  lista_examenes:ConsultaExamen[]=[];
  edicion:boolean=false;
  id:number=0;
  fechaSeleccionada:Date=new Date();
  filteredOptionsEspecialista:Observable<any[]>;  
  especialistaSeleccionado= new Medico();
  especialidadSeleccionada:Especialidad;
  myControlEspecialista:FormControl=new FormControl();
  detalleConsulta:DetalleConsulta[]=[];
  filteredOptionsPaciente:Observable<any[]>;  
  pacienteSeleccionado= new Paciente();
  myControlPaciente:FormControl=new FormControl();
  tratamiento:string;
  diagnostico:string;
  filteredOptionsExamen:Observable<any[]>;  
  examenSeleccionado:Examen;
  medicoSeleccionado:Medico;
  myControlExamen:FormControl=new FormControl();
  idEspecialidadSeleccionado:number;
  idPacienteSeleccionado:number;
  idMedicoSeleccionado:number;
  form:FormGroup;
  mensaje:string;
  
  constructor(private consultaService:ConsultaService,private pacienteService:PacienteService,private builder:FormBuilder,private examenService:ExamenService,private snackBar:MatSnackBar,private especialistaService:EspecialistaService,private route:Router,private router:ActivatedRoute) { 
    this.form=builder.group({
      'id':new FormControl(0),
      'fecha':new FormControl(new Date()),
      'especialista':this.myControlEspecialista,
      'paciente':this.myControlPaciente,
      'examen':this.myControlExamen,
      'diagnostico':new FormControl(''),
      'tratamiento':new FormControl(''),
      
    });
  }
  ngOnInit() {
    this.router.params.subscribe((params: Params)=>{
      this.id=params['id'];
      this.edicion=params['id']!=null;
      this.initForm();      
    });
    this.listarEspecialista();
    this.listarExamen();
    this.listarPaciente();
    this.filteredOptionsEspecialista=this.myControlEspecialista.valueChanges.pipe(map(val=>this.filterEspecialista(val)));
    this.filteredOptionsExamen=this.myControlExamen.valueChanges.pipe(map(val=>this.filterExamen(val)));
    this.filteredOptionsPaciente=this.myControlPaciente.valueChanges.pipe(map(val=>this.filterPaciente(val)));
  }
  filterEspecialista(val:any){
    if(val!=null && val.id>0){
      return this.medicos.filter(opcion => 
        opcion.nombres.toLowerCase().includes(val.nombres.toLowerCase())
        );
    }
    else{
      return this.medicos.filter(opcion => 
        opcion.nombres.toLowerCase().includes(val.toLowerCase()) 
        );
    }
  }
  filterExamen(val:any){
    if(val!=null && val.id>0){
      return this.examenes.filter(opcion => 
        opcion.descripcion.toLowerCase().includes(val.descripcion.toLowerCase())
        );
    }
    else{
      console.log("entro aca");
      return this.examenes.filter(opcion => 
        opcion.descripcion.toLowerCase().includes(val.toLowerCase()) 
        );
    }
  }
  filterPaciente(val:any){
    if(val!=null && val.id>0){
      return this.pacientes.filter(opcion => 
        opcion.nombres.toLowerCase().includes(val.nombres.toLowerCase())
        );
    }
    else{
      return this.pacientes.filter(opcion => 
        opcion.nombres.toLowerCase().includes(val.toLowerCase()) 
        );
    }
  }
  listarEspecialista(){
    return this.especialistaService.listar().subscribe(data=>{
      this.medicos=data;
    });
  }
  listarExamen(){
    return this.examenService.listar().subscribe(data=>{
      console.log(data);
      this.examenes=data;
    });
  }
  listarPaciente(){
    return this.pacienteService.listar().subscribe(data=>{
      this.pacientes=data;
    });
  }
  initForm(){

  }
  
  
  
  displayFnEspecialista(val:Medico){
    return val ? `${val.nombres}` :val; 
  }
  displayFnExamen(val:Examen){
    return val ? `${val.descripcion}` :val; 
  }
  displayFnPaciente(val:Paciente){
    return val ? `${val.nombres}` :val; 
  }
  removerDetalle() {}
  validarForm(){
    if(!this.edicion){
    return this.lista_examenes.length===0 || this.especialistaSeleccionado.nombres.length===0 || this.pacienteSeleccionado.nombres.length===0 ;
    }
  }
  agregar_examen(){
    if(this.examenSeleccionado!=null){
      let cont=0;
      for(let i=0;i<this.lista_examenes.length;i++){
        let examen=this.lista_examenes[i];
        if(examen.examen.id==this.examenSeleccionado.id){
          cont++;
          break;
        }
      }
      if(cont>0){
        this.mensaje=`El examen se encuentra en la lista`;
        this.snackBar.open(this.mensaje,"Aviso",{duration:2000});
  
      }
      else{
      let consultaexamen=new ConsultaExamen();
      consultaexamen.examen=this.examenSeleccionado;
      this.lista_examenes.push(consultaexamen);
      }
    }
    else{
      this.mensaje=`Debe agregar un examen`;
      this.snackBar.open(this.mensaje,"Aviso",{duration:2000});
    }
  }
  validarDetalle(){


  }
  operar(){}   
  seleccionarEspecialista(e:any){
    this.especialistaSeleccionado=e.option.value;  
    console.log(e.option.value);
    
  }
  seleccionarExamen(e:any){
    this.examenSeleccionado=e.option.value;  
    console.log(e.option.value);
    
  }  
  seleccionarPaciente(e:any){
    //console.log(e.option.value);
    this.pacienteSeleccionado=e.option.value;

  }
  aceptar(){
    let medico=new Medico();
    medico.id=this.idMedicoSeleccionado;
    let especialidad=new Especialidad();
    especialidad.id=this.idEspecialidadSeleccionado;
    let paciente=new Paciente()
    //paciente.id=this.idPacienteSeleccionado;
    let consulta=new Consulta();
    consulta.especialidad=especialidad;
    consulta.medico=medico;
    consulta.paciente=paciente;
    let tzoffset=(this.fechaSeleccionada).getTimezoneOffset()*60000;
    let localIsoTime=(new Date(Date.now()-tzoffset)).toISOString();
    consulta.fecha=localIsoTime;
    consulta.detalleConsulta=this.detalleConsulta;
    //console.log(consulta);
    let consultaListaExamen=new ConsultaListaExamen();
    consultaListaExamen.consulta=consulta;
   // consultaListaExamen.lstExamen=this.examenesSeleccionados;
    console.log(consultaListaExamen);
    /* this.consultaService.registrar(consultaListaExamen).subscribe(data=>{
      this.snackBar.open("Se registró","Aviso",{duration:2000});
      setTimeout(()=>{
        this.limpiarControles();
      },2000);
      
    }); */
  }
  limpiarControles(){
    this.detalleConsulta=[];
    this.lista_examenes=[];
    this.tratamiento='';
    this.diagnostico='';
    this.pacienteSeleccionado=new Paciente();
    this.especialidadSeleccionada=new Especialidad();
    this.medicoSeleccionado=new Medico();
    //this.idExamenSeleccionado=0;
    this.fechaSeleccionada=new Date();
    this.fechaSeleccionada.setHours(0);
    this.fechaSeleccionada.setMinutes(0);
    this.fechaSeleccionada.setSeconds(0);
    this.fechaSeleccionada.setMilliseconds(0);
    this.mensaje='';
  }
  agregar(){
    if(this.diagnostico!=null && this.tratamiento!=null){
      let det=new DetalleConsulta();
      det.diagnostico=this.form.value['diagnostico'];
      det.tratamiento=this.form.value['tratamiento'];
      this.detalleConsulta.push(det);
      this.diagnostico='';
      this.tratamiento='';
    }
    else{
      this.mensaje=`Debe agregar un diagnostico y tratamiento`;
      this.snackBar.open(this.mensaje,"Aviso",{duration:2000});
    }

}
removerDiagnostico(index:number){

  console.log(index);
  this.detalleConsulta.splice(index,1);

}
removerExamen(index:number){

  console.log(index);
  this.lista_examenes.splice(index,1);

}
agregarExamen(){
  if(this.examenSeleccionado){
    let cont=0;
    for(let i=0;i<this.lista_examenes.length;i++){
      let examen=this.lista_examenes[i];
      if(examen.examen.id==this.examenSeleccionado.id){
        cont++;
        break;
      }
    }
    if(cont>0){
      this.mensaje=`El examen se encuentra en la lista`;
      this.snackBar.open(this.mensaje,"Aviso",{duration:2000});

    }
    else{
      let consultaexamen=new ConsultaExamen();
      consultaexamen.examen=this.examenSeleccionado;
      this.lista_examenes.push(consultaexamen);
    }
  }
  else{
    this.mensaje=`Debe agregar un examen`;
    this.snackBar.open(this.mensaje,"Aviso",{duration:2000});
  }
}
}
