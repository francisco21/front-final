import { Medico } from './../../../model/especialista';
import { EspecialidadService } from './../../../services/especialidad.service';
import { MatSnackBar } from '@angular/material';
import { EspecialistaService } from './../../../services/especialista.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { Especialidad } from './../../../model/especialidad';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MedicoEspecialidad } from 'src/app/model/medicoespecialidad';
import { map } from 'rxjs/operators';
@Component({
  selector: 'app-especialista-edicion',
  templateUrl: './especialista-edicion.component.html',
  styleUrls: ['./especialista-edicion.component.css']
})
export class EspecialistaEdicionComponent implements OnInit {
  form:FormGroup;
  id:number;
  medico:Medico;
  edicion:boolean=false;
  constructor(private route:ActivatedRoute,private router:Router,private especialistaService:EspecialistaService) { 
    this.form=new FormGroup({
      'id': new FormControl(0),
      'run':new FormControl(''),
      'nombres':new FormControl(''),
      'apellidos':new FormControl(''), 
      'direccion':new FormControl(''),
      'telefono':new FormControl(''),
      'email':new FormControl(''),           
    }); 
  }
  initForm(){
    if(this.edicion){
      this.especialistaService.listarPorId(this.id).subscribe(data=>{
        this.form=new FormGroup({
          'id': new FormControl(data.id),
          'run':new FormControl(data.run),
          'nombres':new FormControl(data.nombres),
          'apellidos':new FormControl(data.apellidos), 
          'direccion':new FormControl(data.direccion),
          'telefono':new FormControl(data.telefono),
          'email':new FormControl(data.email),
        });
      })
        
    }
  }
  ngOnInit() {
    this.medico=new Medico();
    this.route.params.subscribe((params: Params)=>{
      this.id=params['id'];
      this.edicion=params['id']!=null;
      this.initForm();      
    });
  }
  operar(){
    this.medico.id=this.form.value['id'];
    this.medico.run=this.form.value['run'];
    this.medico.nombres=this.form.value['nombres'];
    this.medico.apellidos=this.form.value['apellidos'];
    this.medico.direccion=this.form.value['direccion'];
    this.medico.telefono=this.form.value['telefono'];
    this.medico.email=this.form.value['email'];        
    if(this.edicion){

      this.especialistaService.modificar(this.medico).subscribe(data=>
      {
        this.especialistaService.listar().subscribe(especialistas => {this.especialistaService.medicoCambio.next(especialistas);       
        this.especialistaService.mensajeCambio.next("Se realizo el cambio de manera exitosa!");
      });
    });
    
    }
    else{
      this.especialistaService.registar(this.medico).subscribe(data=>{
        this.especialistaService.listar().subscribe(especialistas => {this.especialistaService.medicoCambio.next(especialistas);       
        this.especialistaService.mensajeCambio.next("Guardado exitoso!");
        });
      });
      
    }
    this.router.navigate(['especialista']);
  }

}