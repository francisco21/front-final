import { EspecialistaService } from './../../services/especialista.service';
import { ActivatedRoute } from '@angular/router';
import { Medico } from './../../model/especialista';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-especialista',
  templateUrl: './especialista.component.html',
  styleUrls: ['./especialista.component.css']
})
export class EspecialistaComponent implements OnInit {

  cantidad:number;
  dataSource:MatTableDataSource<Medico>;
  displayedColumns=['id','run','nombres','apellidos','direccion','telefono','email','acciones'];
  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(public route:ActivatedRoute,private especialistaService:EspecialistaService,private snackBar:MatSnackBar) { }

  ngOnInit() {
    this.especialistaService.medicoCambio.subscribe(data => {
      this.dataSource=new MatTableDataSource(data);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
    this.especialistaService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data,'Aviso',{duration:2000});
    });
    this.especialistaService.listar().subscribe(data=>{
      this.dataSource=new MatTableDataSource(data);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
    
  }
  applyFilter(filterValue:string){
    filterValue=filterValue.trim();
    filterValue=filterValue.toLowerCase();
    this.dataSource.filter=filterValue;
  }
  eliminar(id:number){
    this.especialistaService.eliminar(id).subscribe(data => {
      this.especialistaService.listar().subscribe(data =>{
        this.especialistaService.medicoCambio.next(data);
        this.especialistaService.mensajeCambio.next('Se elimino');
      });
    });
  }
  /*mostarMas(e:any){
    console.log(e);
    return this.especialistaService.listarPageable(e.pageIndex,e.pageSize).subscribe(data => {
      let pacientes=JSON.parse(JSON.stringify(data)).content;
      this.dataSource=new MatTableDataSource(especialistas);
      this.cantidad=JSON.parse(JSON.stringify(data)).totalElements;
      this.dataSource.sort=this.sort;
    });
  }*/

}
